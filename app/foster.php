<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class foster extends Model
{
    //

    protected $appends=['created_date'];

    public function  getCreatedDateAttribute(){
        return $this->created_date();
    }

    public function created_date(){
        return date('D,d-M-Y',strtotime($this->created_at));
    }

    public function user(){
        return $this->belongsTo('App\User', 'user_id','id');
    }

    public function vaccinations(){
        return $this->hasMany('App\vaccination', 'foster_id', 'id');
    }

    public function weights(){
        return $this->hasMany('App\weight', 'foster_id', 'id');
    }

    public function urinations(){
        return $this->hasMany('App\urination', 'foster_id', 'id');
    }
    public function feedings(){
        return $this->hasMany('App\feeding', 'foster_id', 'id');
    }
    public function dewormers(){
        return $this->hasMany('App\dewormer', 'foster_id', 'id');
    }

    public function developments(){
        return $this->hasMany('App\development', 'foster_id', 'id');
    }
    public function defecations(){
        return $this->hasMany('App\defecation', 'foster_id', 'id');
    }
    public function others(){
        return $this->hasMany('App\others', 'foster_id', 'id');
    }
}
