<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class subcription extends Model
{
    //

    protected $hidden=['plan_id'];

    protected $appends=['days_left','started_now'];

    public function getDaysLeftAttribute(){
        $next=date('Y-m-d',strtotime($this->next_billing_time));
        $current=date('Y-m-d');
        $diff=date_diff(date_create($current),date_create($next))->days;

        return $diff > 0 ? $diff :0;
    }

    public function getStartedNowAttribute(){
        $next=date('Y-m-d',strtotime($this->next_billing_time));
        $created=date('Y-m-d',strtotime($this->created_at));
        $diff=date_diff(date_create($created),date_create($next))->days;

        return $diff;
    }
}
