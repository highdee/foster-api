<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class defecation extends Model
{
    protected $appends=['date_r'];

    public function  getDateRAttribute(){
        return $this->date_r();
    }

    public function date_r(){
        return date('D, d-M-Y',strtotime($this->date));
    }
    //
}
