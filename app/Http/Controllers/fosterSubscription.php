<?php

namespace App\Http\Controllers;

use App\subcription;
use App\subtoken;
use App\User;
use Illuminate\Http\Request;

class fosterSubscription extends Controller
{
    //

    public function index($token){
        $data=subtoken::where(['token'=>$token,'status'=>0])->first();

        if(!$data){
            return view('404');
        }
        $user=User::find($data->user_id);

        session()->put('sub-code',$token);

        return view('subscription')->with(['name'=>$user->username]);
    }

    public function success(){

        return view('success');
    }


    public function login(){

        $ch = curl_init('https://api.paypal.com/v1/oauth2/token');
        $additionalHeaders=array(
            'Host: api.paypal.com',
            'Content-type: Application/json',
            "Accept: Application/json",
        );

        $username=env('CLIENT');
        $password=env('SECRET');

        curl_setopt($ch, CURLOPT_HTTPHEADER,$additionalHeaders);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=client_credentials");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $result=json_decode(utf8_encode($result),true);

        session()->put('access_token',$result['access_token']);
    }

    public function verifyToken(Request $request){

        $this->validate($request,[
            'code'=>'required'
        ]);

        $token=session()->get('sub-code');
        $code=$request->input('code');

        if(!isset($token)){
            return redirect()->back();
        }



        $this->login();

        $subtoken=subtoken::where(['token'=>$token,'status'=>0])->first();



        $access_token=session()->get('access_token');


        $headers = array(
            'Authorization: Bearer '.$access_token,
            'Content-type: Application/json'
        );

        $url='https://api.paypal.com/v1/billing/subscriptions/'.$code;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result=curl_exec($ch);
        curl_close($ch);
        $data=json_decode($result);

        if(!isset($data->status)){
            return redirect()->back();
        }

        if($data->status == 'ACTIVE'){
            $subs=subcription::where(['user_id'=>$subtoken->user_id])->get();
            foreach ($subs as $sub){
                $sub->delete();
            }

            $sub=new subcription();
            $sub->user_id=$subtoken->user_id;
            $sub->reference=$data->id;
            $sub->plan_id=$data->plan_id;
            $sub->next_billing_time=$data->billing_info->next_billing_time;
            $sub->save();

            $subtoken->delete();

            session()->remove('sub-code');
            session()->remove('access_token');

            session()->flash('success',true);

            return redirect()->to('success');
        }
//        dd($data);
    }

}
