<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\foster;
use PDF;

class userController extends Controller
{
    public function get(){
        $s=foster::find(1);
        // dd($s);
        // dd($s->fosters[0]->weights);
        // foreach($s->fosters as $key=>$val){
        //     echo($s->fosters[$key]->weights);
        // }
        return view('user')->with('foster',$s);
    }
    // date_format(date_create($val->date),"D, d M Y")
    public function print(){
        $foster=foster::find(1);
        $data=[
            'foster'=>$foster
        ];
        $pdf = PDF::loadView('user',$data);  
        return $pdf->download('medium.pdf');
    }
}
