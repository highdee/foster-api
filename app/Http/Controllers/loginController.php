<?php

namespace App\Http\Controllers;
 use App\Mail\forgotPassword;
 use App\User;
use Illuminate\Http\Request;
 use Illuminate\Support\Facades\Auth;
 use Illuminate\Support\Facades\Mail;
 use JWTAuth;

class loginController extends Controller
{
    //

    public function create_account(Request $request){
        $this->validate($request,[
            'email'=>'required|email|unique:users',
            'username'=>'required',
            'country'=>'required',
            'postal'=>'required',
            'password'=>'required'
        ]);

        $details=$request->input();


        $User=new User();
        $User->username=$details['username'];
        $User->country=$details['country'];
        $User->email=$details['email'];
        $User->postal=$details['postal'];
//        $User->device=$details['device'];
        $User->status=1;
        $User->password=bcrypt($details['password']);
        $User->save();

        // APP_URL_FRONTEND
//        $link=env('APP_URL_FRONTEND')."/verify-account?code=".$User->remember_token;
//        Mail::to($details['email'])->sendNow(new verificationEmail([
//            'link'=>$link
//        ]));

        return response()->json([
            'status'=>true,
            'msg'=>"Your registration was successfull"
        ]);
    }


    public function login(Request $request){
        $this->validate($request,[
            'email'=>'required|email',
            'password'=>'required'
        ]);

        $credentials = $request->only('email', 'password');

        if($token=JWTAuth::attempt($credentials)){
            $user=User::where('id',auth::user($token)->id)->first();
            return response()->json([
                'status'=>true,
                'msg'=>"Your login was successfull",
                'token'=>$token,
                "user"=>$user
            ]);
        }else{
            return response()->json(
                [
                    "status"=>false,
                    "message"=>"Invalid login details"
                ]
            );
        }
    }

    public function sendMail(Request $request){
        $this->validate($request,[
            'email'=>'required|email',
        ]);

        $user=User::where('email',$request->input('email'))->first();

        if(!$user){
            return response()->json(
                [
                    "status"=>false,
                    "message"=>"Invalid email address"
                ]
            );
        }

        $user->remember_token=mt_rand(1000,9999);
        Mail::to($user->email)->send(new forgotPassword( ['code'=>$user->remember_token,'user'=>$user->username] ) );

        $user->save();

        return response()->json(
            [
                "status"=>true,
                "message"=>"Mail was sent",
            ]
        );
    }

    public function verifyCode(Request $request){
        $this->validate($request,[
            'code'=>'required|numeric',
        ]);

        $user=User::where(['email'=>$request->input('email'),'remember_token'=>$request->input('code')])->first();

        if(!$user){
            return response()->json(
                [
                    "status"=>false,
                    "message"=>"Invalid verification code"
                ]
            );
        }
        return response()->json(
            [
                "status"=>true,
                "message"=>"Code verified",
            ]
        );

    }

    public function resetPassword(Request $request){
        $this->validate($request,[
            'password'=>'required|confirmed',
        ]);

        $User=User::where(['email'=>$request->input('email'),'remember_token'=>$request->input('code')])->first();

        if(!$User){
            return response()->json(
                [
                    "status"=>false,
                    "message"=>"Invalid verification code"
                ]
            );
        }

        $User->remember_token=mt_rand(1000,9999);
        $User->password=bcrypt($request->input('password'));
        $User->save();

        return response()->json(
            [
                "status"=>true,
                "message"=>"Password was reset successfully",
            ]
        );

    }
}
