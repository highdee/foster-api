<?php

namespace App\Http\Controllers;

use App\defecation;
use App\development;
use App\dewormer;
use App\feeding;
use App\foster;
use App\others;
use App\subcription;
use App\subtoken;
use App\urination;
use App\User;
use App\vaccination;
use App\weight;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use PDF;

class fosterController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    public function setSubToken(){
        $user=auth::user();

        $token=subtoken::where(['user_id'=>$user->id,'status'=>0])->first();
        if(!$token){
            $token=new subtoken();
        }

        $token->user_id=$user->id;
        $token->token=mt_rand(99999,999999999);
        $token->save();


        $link="http://192.168.43.153:8000/subscription/".$token->token;
        return response([
            'status'=>true,
            'message'=>'Token has been generated',
            'data'=>$link
        ],200);
    }


    public function verifySubToken(){
        $fosterSubscription=new fosterSubscription();
        $fosterSubscription->login();

        $access_token=session()->get('access_token');

        $headers = array(
            'Authorization: Bearer '.$access_token,
            'Content-type: Application/json'
        );

        $user=auth::user();
        $sub=subcription::where(['user_id'=>$user->id])->first();
        if(!$sub){
            return response($user,200);
        }
        $code=$sub->reference;

        $url='https://api.paypal.com/v1/billing/subscriptions/'.$code;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result=curl_exec($ch);
        curl_close($ch);
        $data=json_decode($result);

        if(!isset($data->status)){
            return response('',402);
        }

        $user=auth::user();
        $sub=subcription::where(['reference'=>$code,'user_id'=>$user->id])->first();

        if(!$sub){
            return response('',402);
        }

        if($data->status == 'ACTIVE'){
            $sub->reference=$data->id;
            $sub->plan_id=$data->plan_id;
            $sub->status=0;
            $sub->next_billing_time=$data->billing_info->next_billing_time;
        }else if($data->status == 'SUSPENDED'){
            $sub->status=1;
        }
        else{
            $sub->status=2;
        }

        $sub->save();
        $user=User::find($user->id);

        return $user;
    }

    public function suspendToken($code){
        $user=auth::user();
        $sub=subcription::where(['reference'=>$code,'user_id'=>$user->id])->first();

        if($sub){
            $fosterSubscription=new fosterSubscription();
            $fosterSubscription->login();

            $access_token=session()->get('access_token');

            $headers = array(
                'Authorization: Bearer '.$access_token,
                'Content-type: Application/json'
            );

            $url='https://api.paypal.com/v1/billing/subscriptions/'.$code.'/cancel';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $result=curl_exec($ch);
            curl_close($ch);
            $data=json_decode($result);

            $sub->delete();
            $sub->save();
            $user=User::find($user->id);

            return $user;

        }
    }

    public function activeSub($code){
        $user=auth::user();
        $sub=subcription::where(['reference'=>$code,'user_id'=>$user->id])->first();

        if($sub){
            $fosterSubscription=new fosterSubscription();
            $fosterSubscription->login();

            $access_token=session()->get('access_token');

            $headers = array(
                'Authorization: Bearer '.$access_token,
                'Content-type: Application/json'
            );


            $fields=[
                'reason'=>''
            ];
            $url='https://api.paypal.com/v1/billing/subscriptions/'.$code.'/activate';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch,CURLOPT_POST,true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $result=curl_exec($ch);
            curl_close($ch);
            $data=json_decode($result);

//            return $data;

            $sub->status=0;
            $sub->save();

            $user=User::find($user->id);
            return $user;

        }
    }

    public function cancelSub($code){
        $user=auth::user();
        $sub=subcription::where(['reference'=>$code,'user_id'=>$user->id])->first();

        if($sub){
            $fosterSubscription=new fosterSubscription();
            $fosterSubscription->login();

            $access_token=session()->get('access_token');

            $headers = array(
                'Authorization: Bearer '.$access_token,
                'Content-type: Application/json'
            );


            $fields=[
                'reason'=>''
            ];
            $url='https://api.paypal.com/v1/billing/subscriptions/'.$code.'/cancel';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch,CURLOPT_POST,true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $result=curl_exec($ch);
            curl_close($ch);
            $data=json_decode($result);
            $sub->status=2;
            $sub->save();

            $user=User::find($user->id);
            return $user;

        }
    }

    public function update_account(Request $request,$id){
        $this->validate($request,[
            'email'=>'required',
            'username'=>'required',
            'country'=>'required',
            'postal'=>'required',
        ]);

        $details=$request->input();


        $User=User::find($id);
        if(!$User){
            return response()->json([
                'status'=>false,
                'msg'=>"Account not found"
            ]);
        }
        $User->username=$details['username'];
        $User->country=$details['country'];
        $User->email=$details['email'];
        $User->postal=$details['postal'];

        if(isset($details['password']) && isset($details['new_password'])){
            if(password_verify($details['password'],$User->password)){
                $User->password=bcrypt($details['new_password']);
            }else{
                return response()->json([
                    'status'=>false,
                    'msg'=>"Password not correct"
                ]);
            }
        }
        $User->save();


        return response()->json([
            'status'=>true,
            'msg'=>"Your update was successfull",
            'data'=>$User
        ]);
    }

    public function create_foster(Request $request){
        $this->validate($request,[
            'name'=>'required',
            'species'=>'required',
            'dob'=>'required'
        ]);

        $user=auth::user();
        $data=$request->input();


        $foster=new foster();
        $foster->user_id=$user->id;
        $foster->name=$data['name'];
        $foster->species=$data['species'];
        $foster->dob=$data['dob'];

        $foster->save();


        return response([
            'status'    => true,
            'message'   => "Foster was created successfully",
            'data'=> $foster
        ]);
    }

    public function update_foster(Request $request,$id){
        $user=auth::user();
        $data=$request->input();

        $foster=foster::find($id);

        if(isset($data['agency'])){
            $foster->agency=$data['agency'];
        }

        if(isset($data['intake'])){
            $foster->arrival_date=$data['intake'];
        }

        if(isset($data['exit'])){
            $foster->departure_date=$data['exit'];
        }

        if(isset($data['note'])){
            $foster->note=$data['note'];
        }

        if(isset($data['special_note'])){
            $foster->special_note=$data['special_note'];
        }

        $foster->save();

        return response([
            'status'    => true,
            'message'   => "Foster was updated successfully",
            'data'=> $foster
        ]);
    }
    public function getItems(){
        $foods=foster::where('status',1)->paginate(10);

        return $foods;
    }

    public function uploadImage(Request $request , $id){
        $user=auth::user();
        $data=$request->input();

        $foster=foster::find($id);

        if($request->input("image")) {
            $file = $request->input('image');
            $file = base64_decode($file);
            $imagename = date('dmyhis') . mt_rand(0, 99999) . '' . str_replace('-', '', $request->input('filename'));
            file_put_contents(storage_path() . "/foster/" . $imagename, $file);
            $filename = "http://" . $request->getHttpHost() . "/foster/" . $imagename;
            $foster->filename=$filename;
        }

        $foster->save();
        return response([
            'status'    => true,
            'message'   => "Foster was updated successfully",
            'data'=> $foster
        ]);
    }

    public function createFosterWeight(Request $request,$id){
        $this->validate($request,[
            'date'=>'required',
            'time'=>'required',
            'pre_weight'=>'required',
            'post_weight'=>'required',
            'u_m'=>'required'
        ]);

        $data=$request->input();

        $weight=new weight();
        $weight->foster_id=$id;
        $weight->date=$data['date'];
        $weight->time=$data['time'];
        $weight->pre_weight=$data['pre_weight'];
        $weight->post_weight=$data['post_weight'];
        $weight->u_m=$data['u_m'];

        $weight->save();

        return response([
            'status'    => true,
            'message'   => "Foster was updated successfully",
            'data'=> $weight
        ]);
    }
    public function getFosterWeight($id){
        $foods=weight::where('foster_id',$id)->paginate(10);

        return $foods;
    }


    public function createFosterUrination(Request $request,$id){
        $this->validate($request,[
            'date'=>'required',
            'time'=>'required',
            'response'=>'required',
        ]);

        $data=$request->input();

        $urination=new urination();
        $urination->foster_id=$id;
        $urination->date=$data['date'];
        $urination->time=$data['time'];
        $urination->response=$data['response'];

        $urination->save();

        return response([
            'status'    => true,
            'message'   => "Foster was updated successfully",
            'data'=> $urination
        ]);
    }

    public function getFosterUrination($id){
        $foods=urination::where('foster_id',$id)->paginate(10);

        return $foods;
    }

    public function createFosterDefecation(Request $request,$id){
        $this->validate($request,[
            'date'=>'required',
            'time'=>'required',
            'type'=>'required',
        ]);

        $data=$request->input();

        $defecation=new defecation();
        $defecation->foster_id=$id;
        $defecation->date=$data['date'];
        $defecation->time=$data['time'];
        $defecation->type=$data['type'];

        $defecation->save();

        return response([
            'status'    => true,
            'message'   => "Foster was updated successfully",
            'data'=> $defecation
        ]);
    }

    public function getFosterDefecation($id){
        $foods=defecation::where('foster_id',$id)->paginate(10);

        return $foods;
    }

    public function createFosterFeeding(Request $request,$id){
        $this->validate($request,[
            'date'=>'required',
            'time'=>'required',
            'type'=>'required',
        ]);

        $data=$request->input();

        $feeding=new feeding();
        $feeding->foster_id=$id;
        $feeding->date=$data['date'];
        $feeding->time=$data['time'];
        $feeding->type=$data['type'];
        $feeding->amount=$data['amount'];
        $feeding->unit=$data['unit'];


        $feeding->save();

        return response([
            'status'    => true,
            'message'   => "Feeding was updated successfully",
            'data'=> $feeding
        ]);
    }

    public function getFosterFeeding($id){
        $foods=feeding::where('foster_id',$id)->paginate(10);

        return $foods;
    }

    public function createFosterDevelopment(Request $request,$id){
        $this->validate($request,[
            'date'=>'required',
            'note'=>'required',
        ]);

        $data=$request->input();

        $development=new development();
        $development->foster_id=$id;
        $development->date=$data['date'];
        $development->note=$data['note'];

        $development->save();

        return response([
            'status'    => true,
            'message'   => "Development was updated successfully",
            'data'=> $development
        ]);
    }

    public function getFosterDevelopment($id){
        $development=development::where('foster_id',$id)->paginate(10);

        return $development;
    }

    public function createFosterVaccination(Request $request,$id){
        $this->validate($request,[
            'date'=>'required',
            'type'=>'required',
        ]);

        $data=$request->input();

        $vaccination=new vaccination();
        $vaccination->foster_id=$id;
        $vaccination->date=$data['date'];
        $vaccination->text=$data['type'];

        $vaccination->save();

        return response([
            'status'    => true,
            'message'   => "Vaccination was updated successfully",
            'data'=> $vaccination
        ]);
    }

    public function getFosterVaccination($id){
        $vaccination=vaccination::where('foster_id',$id)->paginate(10);

        return $vaccination;
    }

    public function createFosterDewormer(Request $request,$id){
        $this->validate($request,[
            'date'=>'required',
            'type'=>'required',
        ]);

        $data=$request->input();

        $dewormer=new dewormer();
        $dewormer->foster_id=$id;
        $dewormer->date=$data['date'];
        $dewormer->text=$data['type'];

        $dewormer->save();

        return response([
            'status'    => true,
            'message'   => "Dewormer was updated successfully",
            'data'=> $dewormer
        ]);
    }

    public function getFosterDewormer($id){
        $dewormer=dewormer::where('foster_id',$id)->paginate(10);

        return $dewormer;
    }

    public function createFosterOther(Request $request,$id){
        $this->validate($request,[
            'date'=>'required',
            'type'=>'required',
        ]);

        $data=$request->input();

        $other=new others();
        $other->foster_id=$id;
        $other->date=$data['date'];
        $other->text=$data['type'];

        $other->save();

        return response([
            'status'    => true,
            'message'   => "Other was updated successfully",
            'data'=> $other
        ]);
    }

    public function getFosterOther($id){
        $others=others::where('foster_id',$id)->paginate(10);

        return $others;
    }

    public function sendPdfData(Request $request,$id){
        $this->validate($request,[
            'email'=>'required|email',
        ]);
        $foster=foster::find($id);

        if(!$foster){
            return response([
                'status'    => false,
                'message'   => "Data not found",
            ]);
        }
        $data=[
            'foster'=>$foster
        ];

        $pdf = PDF::loadView('user',$data);
        $filename=storage_path().'/pdf/'.$foster->name.'-'.date('Y-M-d').'.pdf';
        file_put_contents($filename,$pdf->download('medium.pdf'));

        $data=[
            'name'=>$foster->name,
            'email'=>$request->input('email'),
            'path'=>$filename
        ];

        Mail::send("pdf-mail",$data,function($msg) use ($data){
            $msg->to($data['email'])->subject('Foster data for '.$data['name']);
            $msg->attach($data['path'],['name'=>$data['name']]);
        });


        return response([
            'status'    => true,
            'message'   => "Pdf has been sent successsfully",
        ]);
    }
}
