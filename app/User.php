<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $appends=['subscription'];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    protected function getSubscriptionAttribute(){
        return $this->subscription()->first();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function fosters(){
        return $this->hasMany('App\foster','user_id', 'id');
    }

    public function subscription(){
        return $this->hasMany('App\subcription','user_id', 'id');
    }
}
