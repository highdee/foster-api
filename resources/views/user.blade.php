<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    {{-- <link rel="stylesheet" href="{{asset('assets/style.css')}}"> --}}
    <title>Document</title>
    <style>
        .heading{
            border-bottom:1px solid black;
            margin-top: 0;
        }
        .heading h1{
            text-align: right;
            margin-right: 20px;
            margin-bottom: 0;
            margin-top: 10px;
            font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;
        }
        /* .body .col2{
            width: 100%;
            max-width: 400px;
            height: auto;
            float right
        } */
       
        .body tr{
            font-size: 20px;
        }
        table{
            margin-top: 10px;
        }
        .cb{
            display: flex;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="user">
            <a href="/printPdf"> Print PDF</a>
            <div class="heading">
                <h1>Foster</h1>
            </div>
            <div class="body">
                <table cellpadding="9" style="text-align: left;" width="100%">
                    <tr>
                        <th>Name:</th>
                        <td>{{$foster->name}}</td>
                        <?php $pix = explode('/foster/',$foster->filename)[1];  ?>
                        {{-- {{$foster->filename}} --}}
                        <td><img src="foster/{{$pix}}" alt="" width="100px" style="float:right;"></td>
                        {{-- <td> <img src="{{$foster->filename}}" alt=""> </td> --}}
                    </tr>
                    <tr>
                        <th>Dob:</th>
                        <td>{{date_format(date_create($foster->dob),"D, d M Y")}}</td>
                        <td></td>
                    </tr>
                    <tr>
                        <th>Species:</th>
                        <td>{{$foster->species}}</td>
                        <td></td>
                    </tr>
                    <tr>
                        <th>Intake Date:</th>
                        <td>{{$foster->arrival_date}}</td>
                        <td></td>
                    </tr>
                    <tr>
                        <th>Agency:</th>
                        <td>{{$foster->agency}}</td>
                        <td></td>
                    </tr>
                    <tr>
                        <th>Exit Date:</th>
                        <td>{{$foster->departure_date}}</td>
                        <td></td>
                    </tr>
                    <tr>
                        <th> Note:</th>
                        <td>{{$foster->note}}</td>
                        <td></td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="feeding">
            <div class="heading">
                <h1>Feeding</h1>
            </div>
            <table style="width:100%" border="1" cellspacing="0" cellpadding="7">
                <tr>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Type</th>
                    <th>Amount</th>
                    <th>Unit</th>
                </tr>
                @foreach ($foster->feedings as $val)
                    <tr>
                        <td>{{date_format(date_create($val->date),"D, d M Y")}}</td>
                        <td>{{$val->time}}</td>
                        <td>{{$val->type}}</td>
                        <td>{{$val->amount}}</td>
                        <td>{{$val->unit}}</td>
                    </tr>
                @endforeach
            <table>
        </div>

        <div class="weight">
            <div class="heading">
                <h1>Weight</h1>
            </div>
            <table style="width:100%" border="1" cellspacing="0" cellpadding="7">
                <tr>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Pre Weight</th>
                    <th>Post Weight</th>
                    <th>U_M</th>
                </tr>
                @foreach ($foster->weights as $val)
                    <tr>
                        <td>{{date_format(date_create($val->date),"D, d M Y")}}</td>
                        <td>{{date('h:i',strtotime($val->time))}}</td>
                        <td>{{$val->pre_weight}}</td>
                        <td>{{$val->post_weight}}</td>
                        <td>{{$val->u_m}}</td>
                    </tr>
                @endforeach
            <table>
        </div>

        <div class="vaccination">
            <div class="heading">
                <h1>Vaccination</h1>
            </div>
            <table style="width:100%" border="1" cellspacing="0" cellpadding="7">
                <tr>
                    <th>Date</th>
                    <th>Text</th>
                    <th>Type</th>
                </tr>
                @foreach ($foster->vaccinations as $val)
                    <tr>
                        <td>{{date_format(date_create($val->date),"D, d M Y")}}</td>
                        {{-- <td>{{$val->time}}</td> --}}
                        <td>{{$val->text}}</td>
                        <td></td>
                    </tr>
                @endforeach
            <table>
        </div>

        <div class="urination">
            <div class="heading">
                <h1>Urination</h1>
            </div>
            <table style="width:100%" border="1" cellspacing="0" cellpadding="7">
                <tr>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Yes/No</th>
                </tr>
                @foreach ($foster->urinations as $val)
                    <tr>
                        <td>{{date_format(date_create($val->date),"D, d M Y")}}</td>
                        <td>{{date('h:i',strtotime($val->time))}}</td>
                        <td>{{$val->response}}</td>
                    </tr>
                @endforeach
            <table>
        </div>

        <div class="dewormer">
            <div class="heading">
                <h1>Dewormer</h1>
            </div>
            <table style="width:100%" border="1" cellspacing="0" cellpadding="7">
                <tr>
                    <th>Date</th>
                    <th>Type</th>
                </tr>
                @foreach ($foster->dewormers as $val)
                    <tr>
                        <td>{{date_format(date_create($val->date),"D, d M Y")}}</td>
                        <td>{{$val->text}}</td>
                    </tr>
                @endforeach
            <table>
        </div>
        <div class="development">
            <div class="heading">
                <h1>Development</h1>
            </div>
            <table style="width:100%" border="1" cellspacing="0" cellpadding="7">
                <tr>
                    <th>Date</th>
                    <th>Note</th>
                </tr>
                @foreach ($foster->developments as $val)
                    <tr>
                        <td>{{date_format(date_create($val->date),"D, d M Y")}}</td>
                        <td>{{$val->note}}</td>
                    </tr>
                @endforeach
            <table>
        </div>

        <div class="defecation">
            <div class="heading">
                <h1>Defecation</h1>
            </div>
            <table style="width:100%" border="1" cellspacing="0" cellpadding="7">
                <tr>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Type</th>
                </tr>
                @foreach ($foster->defecations as $val)
                    <tr>
                        <td>{{date_format(date_create($val->date),"D, d M Y")}}</td>
                        <td>{{date('h:i',strtotime($val->time))}}</td>
                        <td>{{$val->type}}</td>
                    </tr>
                @endforeach
            <table>
        </div>
        <div class="other">
            <div class="heading">
                <h1>Other Medication</h1>
            </div>
            <table style="width:100%" border="1" cellspacing="0" cellpadding="7">
                <tr>
                    <th>Date</th>
                    <th>Type</th>
                </tr>
                @foreach ($foster->others as $val)
                    <tr>
                        <td>{{date_format(date_create($val->date),"D, d M Y")}}</td>
                        <td>{{$val->text}}</td>
                    </tr>
                @endforeach
            <table>
        </div>
    </div>
</body>
</html>
