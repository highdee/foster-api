<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <style>
        #center{
            position: absolute;
            top:0;
            bottom:0;
            margin: auto;
            height: fit-content;
            padding: 0px 10px;
            width: fit-content;
            left: 0;
            right: 0;
        }
    </style>
</head>
<body>
   <div id="center">
       <div class="alert alert-success">
           <h4>SUBSCRIPTION ACTIVATED!!</h4>
           <p>Your subscription was successfull</p>
       </div>
   </div>
</body>
</html>
