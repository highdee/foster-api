<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//
//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('foster/{filename}/{width?}/{height?}',function($filename,$width=0,$height=0){
    $img = Image::make(storage_path().'/foster/'.$filename);

    if($width == 0){
        $width=$img->width();
    }
    if($height == 0){
        $height=$img->height();
    }
    return $img->resize($width, $height)->response('jpg');
});
Route::get('foster/{filename}',function($filename){
    $img = Image::make(storage_path().'/foster/'.$filename);

    return $img->response('jpg');
});

Route::get('subscription/{token}',"fosterSubscription@index");
Route::get('success',"fosterSubscription@success");
Route::post('subscription/verify-token',"fosterSubscription@verifyToken");

Route::get('getUser', 'userController@get');
Route::get('printPdf', 'userController@print');
