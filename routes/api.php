<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('v1')->group(function(){

    Route::post('create-account',"loginController@create_account");
    Route::post('login',"loginController@login");


    Route::post('update-account/{id}',"fosterController@update_account");

    Route::post('create-foster',"fosterController@create_foster");
    Route::post('update-foster/{id}',"fosterController@update_foster");
    Route::post('add-image/{id}',"fosterController@uploadImage");

    Route::post('create-foster-weight/{id}',"fosterController@createFosterWeight");
    Route::get('get-foster-weight/{id}',"fosterController@getFosterWeight");

    Route::post('create-foster-urination/{id}',"fosterController@createFosterUrination");
    Route::get('get-foster-urination/{id}',"fosterController@getFosterUrination");

    Route::post('create-foster-defecation/{id}',"fosterController@createFosterDefecation");
    Route::get('get-foster-defecation/{id}',"fosterController@getFosterDefecation");

    Route::post('create-foster-feeding/{id}',"fosterController@createFosterFeeding");
    Route::get('get-foster-feeding/{id}',"fosterController@getFosterFeeding");

    Route::post('create-foster-development/{id}',"fosterController@createFosterDevelopment");
    Route::get('get-foster-development/{id}',"fosterController@getFosterDevelopment");

    Route::get('get-fosters',"fosterController@getItems");

    Route::post('create-foster-vaccination/{id}',"fosterController@createFosterVaccination");
    Route::get('get-foster-vaccination/{id}',"fosterController@getFosterVaccination");

    Route::post('create-foster-dewormer/{id}',"fosterController@createFosterDewormer");
    Route::get('get-foster-dewormer/{id}',"fosterController@getFosterDewormer");

    Route::post('create-foster-others/{id}',"fosterController@createFosterOther");
    Route::get('get-foster-other/{id}',"fosterController@getFosterOther");

    Route::post('create-foster-devel/{id}',"fosterController@createFosterVaccination");
    Route::get('get-foster-vaccination/{id}',"fosterController@getFosterVaccination");
    Route::Post('send-pdf-data/{id}',"fosterController@sendPdfData");

    Route::post('send-code',"loginController@sendMail");
    Route::post('verify-code',"loginController@verifyCode");
    Route::post('reset-password',"loginController@resetPassword");

    Route::get('get-subscription-token',"fosterController@setSubToken");
    Route::get('verify-subscription-token',"fosterController@verifySubToken");
    Route::get('suspend-subscription-token/{code}',"fosterController@suspendToken");
    Route::get('activate-subscription-token/{code}',"fosterController@activeSub");
    Route::get('cancel-subscription-token/{code}',"fosterController@cancelSub");

});
